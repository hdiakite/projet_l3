<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Accueil</title>
</head>
<body class="couleur">
<?php
include("Menue_Principale.php");
?>
<br/>
<div class="row">
    <div class="col-lg-12">
        <a href="ajoutPrestation.php"  class="btn btn-success float-right btn-lg active" role="button">Ajouter une PRESTATION</a>
        <a href="ajoutProduit.php"  class="btn btn-warning float-right btn-lg active" role="button">Ajouter un PRODUIT</a>
    </div>
</div>
<br/>
<br/>
<div class="col-md-3 offset-md-3">
    <form name="prestation_form" method="post" action="" id="prestation_form">
        <div class="form-group row">
            <label for="nom" >Nom Prestation</label>
            <input type="text" name="nom" class="form-control" id="nom" required>
        </div>
        <div class="form-group">
            <label for="sel1">Catégories</label>
            <select class="form-control"  name="categorie" id="sel1">
                <option></option>
                <option value = "Soins corpd" >Soins corps</option>
                <option value = "mise en beaute">Mise en beauté</option>
                <option value = "manicure/pedicure ">Manicure/pedicure</option>
                <option value = "coiffure">Coifure</option>


            </select>
        </div>
        <div class="form-group row">
            <label for="prix" class="col-sm-2 col-form-label">Prix</label>
            <input type="number" name="prix" class="form-control" id="prix" required>
        </div>
        <div class="form-group row">
            <label for="description">Description</label>
            <textarea  name="description" class="form-control" id="description" rows="3"></textarea>
        </div>

        <div>
            <button  type="submit" name="submit"  class="btn btn-secondary btn-lg">Ajouter</button>
        </div>
    </form>
</div>
<br/>
<button class="bouton"><a class="nav-link" href="menu.php">Retoure à l'accueil</a></button>
</body>
</html>

<script>
    $(document).ready(function() {
        $('#prestation_form').on("submit", function(e) {
            e.preventDefault();
            var sendData = $(this).serialize();
            console.log(sendData);
            $.ajax({
                url : "../controleur/prestation_insert.php",
                method : "POST",
                data : sendData,
                //dataType:"text"
                success : function(data) {
                    $(".resp_msg").text(data);
                    $("prestation_form")[0].reset();
                }

            })
        });
    });

</script>




