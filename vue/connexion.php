<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>page_connexion</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body class="couleur">
<?php
session_start();
include("Menue_Principale.php");
?>
<br/>
<br/>
<br/>
<form class="photo" id="connexionForm">


    <div class="col-md-4 offset-md-4">

            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label ">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Mot de pass</label>
                <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                </div>
            </div>

    </div>
    <button type="submit" name="submit" class="btn btn-primary">Connexion</button>
</form>
<div class="connect_msg"></div>
<br/>
<br/>
<br/>
<footer class="pied" style="line-height: 60%; width: 1850px;">
    <br/>

    <p class="cg"> <strong>Conditions générales</strong></p>
    <p> Les conditions générales de vente constituent la base de votre négociation commerciale et de vos relations commerciales.</p>
       <p> Lorsque vous vous lancez dans une activité de vente de produits ou de prestations de service en B to B (Business to Business) </p>
        <p> ou B to C (Business to Customer), vous devez informer vos clients de vos conditions générales de vente. Ces conditions écrites permettent </p>
        <p> de sécuriser vos relations commerciales. Les clauses peuvent être différentes </p>
       <p> selon que vos produits ou services concernent des consommateurs particuliers ou des professionnels. </p>
    <p> desse Postal : 11 Avenue de Gbessia kobébougni </p>
</footer>
<br/>

</body>
</html>


<script>
    $(document).ready(function() {
        $('#connexionForm').on("submit", function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            console.log(data);
            $.ajax({
                url : "../controleur/verification_connexion.php",
                method : "POST",
                data : data,
                //dataType:"text"
                success : function(data) {
                    //$(".connect_msg").text("Connexion réussi");
                    //$(".connect_msg").text(data);
                    //if(data == 0) {
                        //$(".connect_msg").text(data);
                        window.location.href = "menu.php";
                    /*}
                    else {
                        $(".connect_msg").text(data);
                        window.location.href = "menu.php";
                    }*/
                    //window.location.href = "menu.php";
                    $('#connexionForm')[0].reset();
                    //document.getElementById("#inscription-form").reset()
                }

            })
        });
    });

</script>