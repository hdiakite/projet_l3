<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>page_connexion</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body class="couleur">
        <?php
        session_start();
        include("Menue_Principale.php");
        ?>
        <br/>
        <br/>
        <br/>
        <div class="col-md-4 offset-md-4">
            <form name="inscriptionForm" method="post" action="" id="inscriptionForm">

                <div class="form-group row">
                    <label for="nom" class="col-sm-2 col-form-label inscrip">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" name="nom" class="form-control" id="nom" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="prenom" class="col-sm-2 col-form-label">Prenom </label>
                    <div class="col-sm-10">
                        <input type="text" name="prenom" class="form-control" id="prenom" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail4" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="email" placeholder="xxxxx@exemple.com" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Mot de Passe</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="password" placeholder="" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Adresse</label>
                    <div class="col-sm-10">
                        <input type="text" name="address" class="form-control" id="address" placeholder="1234 Main St" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ville" class="col-sm-2 col-form-label">Ville</label>
                    <div class="col-sm-10">
                        <input type="text"  name="ville" class="form-control" id="inputAddress2" placeholder="Conakry" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pays" class="col-sm-2 col-form-label">Pays</label>
                    <div class="col-sm-10">
                        <input type="text"  name="pays" class="form-control" id="pays" placeholder="Guinée" required>
                    </div>
                </div>

                <div class="from-group row">
                    <label from="dateNaiss" class="col-sm-2 col-form-label">Date de naissance</label>
                    <div class="col-sm-10">
                        <input type="date" name="dateNaiss" max="2008-12-31" min="1000-01-01" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label" for="gridCheck">
                            Acceptez les conditions génerales
                        </label>
                    </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Soumettre</button>
            </form>
        </div>
        <div class="resp_msg"></div>
        <br/>
        <br/>
        <br/>
        <footer class="pied" style="line-height: 60%; width: 1850px;">
            <br/>
            <p class="cg"> <strong>Conditions générales</strong></p>
            <p> Les conditions générales de vente constituent la base de votre négociation commerciale et de vos relations commerciales.</p>
            <p> Lorsque vous vous lancez dans une activité de vente de produits ou de prestations de service en B to B (Business to Business) </p>
            <p> ou B to C (Business to Customer), vous devez informer vos clients de vos conditions générales de vente. Ces conditions écrites permettent </p>
            <p> de sécuriser vos relations commerciales. Les clauses peuvent être différentes </p>
            <p> selon que vos produits ou services concernent des consommateurs particuliers ou des professionnels. </p>
            <p> desse Postal : 11 Avenue de Gbessia kobébougni </p>
        </footer>
        <br/>
</body>
</html>

<script>
    $(document).ready(function() {
        $('#inscriptionForm').on("submit", function(e) {
            e.preventDefault();
            var sendData = $(this).serialize();
            console.log(sendData);
            $.ajax({
                url : "../controleur/inscription_insert.php",
                method : "POST",
                data : sendData,
                //dataType:"text"
                success : function(data) {
                    $(".resp_msg").text(data);
                    $("#inscriptionForm")[0].reset();
                }

            })
        });
    });

</script>