<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Accueil</title>
</head>
<body class="couleur">
<?php
include("Menue_Principale.php");
?>
<br/>

<br/>
<br/>
<div class="col-md-3 offset-md-3">
        <form name="ajoutProduitForm" method="post" enctype="multipart/form-data" action="" id="ajoutProduitForm">

            <div class="form-group row">
                <label for="nom" >Nom Produit</label>
                <input type="text" name="nom" class="form-control" id="nom" required>
            </div>

            <div class="form-group">
                <label for="categorie" >Catégories</label>
                <select class="form-control" name="categorie" id="categorie">
                    <option selected>faites votre choix</option>
                    <option value = "Soins de visage">Soins visage</option>
                    <option value="Savons et lait corps">Savons et lait corps</option>
                    <option value="gamme de produit">Gammes de produits</option>
                    <option value="Effets maquillages">Effets maquillages</option>

                </select>
            </div>

            <div class="form-group row">
                <label for="prix" class="col-sm-2 col-form-label">Prix</label>
                <input type="number" name="prix" class="form-control" id="prix" required>
            </div>

            <div class="form-group row">
                <label for="description">Description</label>
                     <textarea  name="description" class="form-control" id="description" rows="3"></textarea>
            </div>

             <div class="form-group">
                <label for="fileToUpload"> Choisir une photo</label>
                <input type="file" name="fileToUpload" class="form-control-file" id="fileToUpload" required>
                 <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
             </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" name="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </div>

</form>
</div>
<div class="resp_msg"></div>
<br/>
<button class="bouton"><a class="nav-link" href="menu.php">Retoure à l'accueil</a></button>
</body>
</html>

<script>
    $(document).ready(function() {
        $('#ajoutProduitForm').on("submit", function(e) {
            e.preventDefault();

            $.ajax({
                url : "../controleur/produit_insert.php",
                type : "POST",
                data : new FormData(this),
                contentType: false,
                processData: false,
                cache:false,
                success : function(data) {
                    $("#ajoutProduitForm")[0].reset();
                    if(data != 0){
                        $(".resp_msg").text(data);
                    }else{
                        alert('Chargement impossible');
                    }
                }

            })
        });
    });

</script>