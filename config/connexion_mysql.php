<?php

$servername = 'localhost';
$username = 'phpmyadminuser';
$password = 'root';
$dbname = 'belleDame';

//On établit la connexion
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");

//On vérifie la connexion
if ($conn->connect_error) {
    die('Erreur : ' . $conn->connect_error);
}
//echo 'Connexion réussie';
